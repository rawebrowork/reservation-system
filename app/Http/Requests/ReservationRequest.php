<?php

namespace App\Http\Requests;

use App\Rules\MaxClients;
use App\Rules\TableCount;
use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'restaurant_id'=> ['required', 'integer'],
            'first_name'=> ['required', 'max:255'],
            'last_name'=> ['required', 'max:255'],
            'email'=> ['required', 'email','max:255'],
            'phone_number'=> ['required', 'regex:/(8|\+370)[0-9]{8}/'],
            'reservation_date'=> ['required', 'date_format:Y-m-d H:00:00', new TableCount()],
            'clients' => [new MaxClients()],
            'clients.*.first_name'=> ['required', 'max:255'],
            'clients.*.last_name'=> ['required', 'max:255'],
            'clients.*.email'=> ['required', 'email', 'max:255'],
        ];
    }
}
