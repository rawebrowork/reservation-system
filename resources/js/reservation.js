$(function($) {
    createElementInForm(
        '#hidden-template',
        'button.add-client',
        '#client-prototype',
        '#no-client-list',
        '#client-list'
    );
    deleteElementInForm('button.remove');
})

function createElementInForm(hiddenTemplate, addButton, prototypeName, noListClass, listClass) {
    const template = $(hiddenTemplate).html();

    $(addButton).click(function() {
        let index = $(listClass).data('client-count');
        index++;

        if( index > 0) {
            $(noListClass).remove();
        }
        $(listClass).append(template.replace(/\{index\}/g, index)).data('client-count', index);
    });
}
function deleteElementInForm(removeButton) {
    $('body').on('click', removeButton, function () {
        $(this).parent().remove();
    });
}
