<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['phone_number', 'reservation_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reservationClient()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(Client::class);
    }

    /**
     * @return int
     */
    public function reservedTableCount(): int
    {
        return $this->newQuery()
            ->where('reservation_date', '=', $this->reservation_date)
            ->count();
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function($reservation) {
            foreach ($reservation->clients as $client) {
                $client->delete();
            }
            $reservation->reservationClient()->delete();
        });
    }
}

