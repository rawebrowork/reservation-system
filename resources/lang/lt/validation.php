<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'reservation' => [
        'max_clients_limit' => 'Rezervacija viršija restorano staliuko maksimalų žmonių skaičių',
        'all_tables_booked' => 'Pasirinkta data nebeturi laisvų staliukų restorane',
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'name' => [
            'required' => 'Restorano vardas yra privalomas',
            'max' => 'Restorano vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
        'table_count' => [
            'required' => 'Staliukų skaičius yra privalomas',
            'integer' => 'Staliukų skaičius turi būti skaičiaus tipo',
            'gt' => 'Staliukų skaičius turi būti daugiau nei 0',
        ],
        'max_clients' => [
            'required' => 'Žmonių skaičius yra privalomas',
            'integer' => 'Žmonių skaičius turi būti skaičiaus tipo',
            'gt' => 'Žmonių skaičius turi būti daugiau nei 0',
        ],
        'restaurant_id' => [
            'required' => 'Restoranas yra privalomas',
            'integer' => 'Restoranas turi būti skaičiaus tipo',
        ],
        'first_name' => [
            'required' => 'Vardas yra privalomas',
            'max' => 'Vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
        'last_name' => [
            'required' => 'Vardas yra privalomas',
            'max' => 'Vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
        'email' => [
            'required' => 'El. paštas yra privalomas',
            'email' => 'El. pašto formatas yra netinkamas',
            'max' => 'Vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
        'phone_number' => [
            'required' => 'Telefono numeris yra privalomas',
            'regex:/(8|\+370)[0-9]{8}/' => 'Telefono numerio formatas yra netinkamas',
        ],
        'reservation_date' => [
            'required' => 'Rezervacijos data yra privaloma',
            'date_format:Y-m-d H:00:00' => 'Rezervacijos datos formatas yra netinkamas, formatatas:Y-m-d H:00:00',
        ],
        'clients.*.first_name' => [
            'required' => 'Vardas yra privalomas',
            'max:255' => 'Vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
        'clients.*.last_name' => [
            'required' => 'Vardas yra privalomas',
            'max' => 'Vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
        'clients.*.email' => [
            'required' => 'El. paštas yra privalomas',
            'email' => 'El. pašto formatas yra netinkamas',
            'max' => 'Vardas turi būti sudarytas iš nedaugiau nei 255 simbolių',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

];
