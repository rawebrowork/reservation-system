require('./bootstrap');

window.jQuery = window.$ = $ = require('jquery');
window.moment = require('moment');
require('tempusdominus-bootstrap-4');
require('./datepicker-loader');
require('./reservation');
