<?php

namespace App\Rules;

use App\Models\Restaurant;
use Illuminate\Contracts\Validation\Rule;

class MaxClients implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value) {
            return true;
        }

        if (!request('restaurant_id')){
            return false;
        }
        $restaurant = Restaurant::find(request('restaurant_id'));

        return $restaurant->max_clients >= count($value) + 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.reservation.max_clients_limit');
    }
}
