<div class="form-group" style="overflow:hidden;">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    <div style="overflow:hidden;">
        <div class="form-group">
            <div class="row">
                <div class="col-md-8">
                    <div class="datetimepicker-load" data-date="{{ $value }}"></div>
                    <input type="hidden" name="{{ $name }}" id="{{ $name }}" value="{{ $value }}">
                </div>
            </div>
        </div>
    </div>
</div>
