<?php

namespace App\Http\Controllers;

use App\Http\Requests\RestaurantRequest;
use App\Models\Restaurant;

class RestaurantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $restaurants = Restaurant::all();

        return view('restaurant.index', compact('restaurants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('restaurant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RestaurantRequest $restaurantRequest
     * @return \Illuminate\Routing\Redirector
     */
    public function store(RestaurantRequest $restaurantRequest)
    {
        $restaurant = new Restaurant($restaurantRequest->validated());
        $restaurant->save();

        if (request('back_to_reservation')) {
            return redirect(route('reservations.create'));
        }

        return redirect(route('restaurants.index'))
            ->with('success', trans('messages.restaurant.saved'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Restaurant $restaurant
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Restaurant $restaurant)
    {
        return view('restaurant.edit', compact('restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RestaurantRequest $restaurantRequest
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Restaurant $restaurant, RestaurantRequest $restaurantRequest)
    {
        $restaurant->update($restaurantRequest->validated());

        return redirect(route('restaurants.index'))
            ->with('success', trans('messages.restaurant.saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Restaurant $restaurant
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Restaurant $restaurant)
    {
        $restaurant->delete();

        return redirect(route('restaurants.index'))
            ->with('success', trans('messages.restaurant.deleted'));
    }
}
