@if ($clients)
    @foreach ($clients as $index => $client)
        <div class="client-box">
            {{ Form::bsText(trans('messages.reservation.client.last_name'), 'clients[' . $index . '][last_name]', $client['last_name']) }}
            {{ Form::bsText(trans('messages.reservation.client.first_name'), 'clients[' . $index . '][first_name]', $client['first_name']) }}
            {{ Form::bsText(trans('messages.reservation.client.email'), 'clients[' . $index . '][email]', $client['email']) }}
            <button type="button" class="remove btn btn-danger">@lang('messages.delete')</button>
            <div class="clear"></div>
        </div>
    @endforeach
@endif
