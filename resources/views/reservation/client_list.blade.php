<div class="client-main">
    @if (!$clients)
        <div id="no-client-list">@lang('messages.reservation.clients_empty')</div>
    @endif
    <div id="client-list" data-client-count="@if ($clients){{ count($clients) }}@else 0 @endif">
        @include('reservation.client_old', ['clients' => $clients])
    </div>
    <button type="button" class="add-client btn btn-success">@lang('messages.reservation.add_client')</button>
</div>
