<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsText', 'components.form.text', ['label', 'name', 'value' => null, 'attributes' => []]);
        Form::component('bsSelect', 'components.form.select', ['label', 'name', 'value' => [], 'selectedValue' => null, 'attributes' => []]);
        Form::component('bsDatepicker', 'components.form.datepicker', ['label', 'name', 'value' => null, 'attributes' => []]);
    }
}
