@extends('../layout/main_layout')

@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">@lang('messages.reservation.add')</h1>
            <div>
                @if (!count($restaurants))
                    <div class="alert alert-danger">
                        <h3>@lang('messages.reservation.no_restaurant')</h3>
                        <div>
                            {{ link_to_action('RestaurantsController@create',
                                trans('messages.restaurant.new'),
                                ['back_to_reservation' => true],
                                [ 'class' => 'btn btn-primary', 'style' => 'margin: 19px;']
                               )
                            }}
                        </div>
                    </div>
                @else
                    @include('shared.errors')
                    {{ Form::open(['action'=> ['ReservationsController@store'], 'method'=>'POST']) }}
                        {{ Form::bsSelect(trans('messages.reservation.restaurant_select'),
                            'restaurant_id',
                            $restaurants,
                            old('restaurant_id'),
                            ['placeholder' => trans('messages.reservation.restaurant_placeholder')])
                        }}
                        {{ Form::bsText(trans('messages.reservation.first_name'), 'first_name', old('first_name')) }}
                        {{ Form::bsText(trans('messages.reservation.last_name'), 'last_name', old('last_name')) }}
                        {{ Form::bsText(trans('messages.reservation.email'), 'email', old('email')) }}
                        {{ Form::bsText(trans('messages.reservation.phone_number'), 'phone_number', old('phone_number')) }}
                        {{ Form::bsDatepicker(trans('messages.reservation.date'), 'reservation_date', old('reservation_date')) }}

                        {{ Form::label('reservation_client', trans('messages.reservation.client.list'), ['class' => 'control-label']) }}
                        @include('reservation.client_list', ['clients' => old('clients')])

                        {{ Form::submit(trans('messages.reservation.add'),['class'=>'btn btn-primary']) }}
                    {{ Form::close() }}
                    @include('reservation.client')
                @endif
            </div>
        </div>
    </div>
@endsection
