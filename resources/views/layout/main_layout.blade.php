<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@lang('messages.title')</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                {{ link_to('/', trans('messages.title'), [ 'class' => 'navbar-brand']) }}
                <button class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown"
                        aria-expanded="false"
                        aria-label="Toggle navigation
                ">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            {{ link_to_action('RestaurantsController@index', trans('messages.nav.restaurants'), [], [ 'class' => 'nav-link']) }}
                        </li>
                        <li class="nav-item">
                            {{ link_to_action('ReservationsController@index', trans('messages.nav.reservations'), [], [ 'class' => 'nav-link']) }}
                        </li>
                    </ul>
                </div>
            </nav>
            @yield('main')
        </div>
        <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    </body>
</html>
