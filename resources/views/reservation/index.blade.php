@extends('../layout/main_layout')

@section('main')
<div class="row">
    <div class="col-sm-12">
        @include('shared.success')
        <h1 class="display-3">@lang('messages.reservation.title')</h1>
        <div>
            {{ link_to_action('ReservationsController@create', trans('messages.reservation.new'), [], [ 'class' => 'btn btn-primary', 'style' => 'margin: 19px;']) }}
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>@lang('messages.id')</td>
                <td>@lang('messages.restaurant.name')</td>
                <td>@lang('messages.reservation.name')</td>
                <td>@lang('messages.reservation.email')</td>
                <td>@lang('messages.reservation.phone_number')</td>
                <td>@lang('messages.reservation.reservation_date')</td>
                <td>@lang('messages.reservation.client_count')</td>
                <td>@lang('messages.reservation.table_count')</td>
                <td colspan = 2>@lang('messages.actions')</td>
            </tr>
            </thead>
            <tbody>
            @foreach($reservations as $reservation)
                <tr>
                    <td>{{$reservation->id}}</td>
                    <td>{{$reservation->restaurant->name}}</td>
                    <td>{{$reservation->reservationClient->first_name}} {{$reservation->reservationClient->last_name}}</td>
                    <td>{{$reservation->reservationClient->email}}</td>
                    <td>{{$reservation->phone_number}}</td>
                    <td>{{$reservation->reservation_date}}</td>
                    <td>{{$reservation->clients->count() + 1}} / {{$reservation->restaurant->max_clients}}</td>
                    <td>{{$reservation->reservedTableCount()}} / {{$reservation->restaurant->table_count}}</td>
                    <td>
                        {{ link_to_action('ReservationsController@edit', trans('messages.edit'), $reservation, [ 'class' => 'btn btn-primary']) }}
                    </td>
                    <td>
                        {{ Form::open(['action'=> ['ReservationsController@destroy', $reservation->id], 'method'=>'DELETE']) }}
                            {{ Form::submit(trans('messages.delete'),['class'=>'btn btn-danger']) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div>
</div>
@endsection
