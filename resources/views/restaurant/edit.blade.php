@extends('../layout/main_layout')

@section('main')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">@lang('messages.restaurant.add')</h1>
            <div>
                @include('shared.errors')
                {{ Form::open(['action'=> ['RestaurantsController@update', $restaurant], 'method'=>'PUT']) }}
                    {{ Form::bsText(trans('messages.restaurant.name'), 'name', $restaurant->name) }}
                    {{ Form::bsText(trans('messages.restaurant.table_count'), 'table_count', $restaurant->table_count) }}
                    {{ Form::bsText(trans('messages.restaurant.max_clients'), 'max_clients', $restaurant->max_clients) }}

                    {{ Form::submit(trans('messages.restaurant.update'),['class'=>'btn btn-primary']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
