$(function () {
    $('.datetimepicker-load').datetimepicker({
        format: 'YYYY-MM-DD HH',
        inline: true,
        sideBySide: true,
        locale: 'lt',
        useCurrent: false,
        defaultDate: $('.datetimepicker-load').data('date')
    });
    $('.datetimepicker-load').on('change.datetimepicker', function(event) {
        let formatted_date = event.date.format('YYYY-MM-DD HH:00:00');
        $(this).parent().children('input').val(formatted_date);
    });
});
