<script id="hidden-template" type="text/x-custom-template">
    <div class="client-box">
        {{ Form::bsText(trans('messages.reservation.client.first_name'), 'clients[{index}][first_name]') }}
        {{ Form::bsText(trans('messages.reservation.client.last_name'), 'clients[{index}][last_name]') }}
        {{ Form::bsText(trans('messages.reservation.client.email'), 'clients[{index}][email]') }}
        <button type="button" class="remove btn btn-danger">@lang('messages.delete')</button>
        <div class="clear"></div>
    </div>
</script>
