@extends('../layout/main_layout')

@section('main')
<div class="row">
    <div class="col-sm-12">
        @include('shared.success')
        <h1 class="display-3">@lang('messages.restaurant.title')</h1>
        <div>
            {{ link_to_action('RestaurantsController@create', trans('messages.restaurant.new'), [], [ 'class' => 'btn btn-primary', 'style' => 'margin: 19px;']) }}
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>@lang('messages.id')</td>
                <td>@lang('messages.restaurant.name')</td>
                <td>@lang('messages.restaurant.table_count')</td>
                <td>@lang('messages.restaurant.max_clients')</td>
                <td colspan = 2>@lang('messages.actions')</td>
            </tr>
            </thead>
            <tbody>
            @foreach($restaurants as $restaurant)
                <tr>
                    <td>{{$restaurant->id}}</td>
                    <td>{{$restaurant->name}}</td>
                    <td>{{$restaurant->table_count}}</td>
                    <td>{{$restaurant->max_clients}}</td>
                    <td>
                        {{ link_to_action('RestaurantsController@edit', trans('messages.edit'), $restaurant, [ 'class' => 'btn btn-primary']) }}
                    </td>
                    <td>
                        {{ Form::open(['action'=> ['RestaurantsController@destroy', $restaurant], 'method'=>'DELETE']) }}
                            {{ Form::submit(trans('messages.delete'),['class'=>'btn btn-danger']) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div>
</div>
@endsection
