<?php

namespace App\Rules;

use App\Models\Reservation;
use App\Models\Restaurant;
use Illuminate\Contracts\Validation\Rule;

class TableCount implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!request('restaurant_id')){
            return false;
        }

        $restaurant = Restaurant::find(request('restaurant_id'));

        $reservationQuery = Reservation::where('reservation_date', $value);
        if(request('reservation')) {
            $reservationQuery->where('id', '!=', request('reservation')->id);
        }
        $reservations = $reservationQuery->get();

        return $restaurant->table_count > $reservations->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.reservation.all_tables_booked');
    }
}
