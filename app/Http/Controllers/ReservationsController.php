<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Http\Requests\ReservationRequest;
use App\Models\Reservation;
use App\Models\Restaurant;
use Illuminate\Http\Request;

class ReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $reservations = Reservation::all();

        return view('reservation.index', compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $restaurants = Restaurant::all()->pluck('name', 'id');

        return view('reservation.create', compact('restaurants'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReservationRequest $reservationRequest
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(ReservationRequest $reservationRequest)
    {
        $validatedData = $reservationRequest->validated();

        $restaurant = Restaurant::find($validatedData['restaurant_id']);

        $reservationClient = new Client();
        $reservationClient->first_name = $validatedData['first_name'];
        $reservationClient->last_name = $validatedData['last_name'];
        $reservationClient->email = $validatedData['email'];
        $reservationClient->save();

        $reservation = new Reservation();
        $reservation->phone_number = $validatedData['phone_number'];
        $reservation->reservation_date = $validatedData['reservation_date'];
        $reservation->reservationClient()->associate($reservationClient);
        $reservation->restaurant()->associate($restaurant);
        $reservation->save();

        $this->clientsCreation($validatedData, $reservation);

        return redirect(route('reservations.index'))
            ->with('success', trans('messages.reservation.saved'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Reservation $reservation
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Reservation $reservation)
    {
        $restaurants = [];
        $restaurantsData = Restaurant::all();
        foreach ($restaurantsData as $restaurant) {
            $restaurants[$restaurant->id] = $restaurant->name . ' | ' .
                trans('messages.restaurant.table_count') . ' ' . $restaurant->table_count . ' | ' .
                trans('messages.restaurant.max_clients') . ' ' . $restaurant->max_clients;
        }

        return view('reservation.edit', compact(['reservation', 'restaurants']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReservationRequest $reservationRequest
     * @param Reservation $reservation
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ReservationRequest $reservationRequest, Reservation $reservation)
    {
        $validatedData = $reservationRequest->validated();

        $restaurant = Restaurant::find($validatedData['restaurant_id']);
        $reservation->restaurant()->associate($restaurant);

        $reservation->reservationClient()->update([
            'first_name' => $validatedData['first_name'],
            'last_name' => $validatedData['last_name'],
            'email' => $validatedData['email'],
        ]);

        foreach ($reservation->clients as $client) {
            $client->delete();
        }

        $this->clientsCreation($validatedData, $reservation);

        $reservation->update([
            'phone_number' => $validatedData['phone_number'],
            'reservation_date' => $validatedData['reservation_date'],
        ]);

        return redirect(route('reservations.index'))
            ->with('success', trans('messages.reservation.saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Reservation $reservation
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->delete();

        return redirect(route('reservations.index'))
            ->with('success', trans('messages.reservation.deleted'));
    }

    /**
     * @param array $validatedData
     * @param Reservation $reservation
     */
    private function clientsCreation(array $validatedData, Reservation $reservation): void
    {
        if (isset($validatedData['clients'])) {
            foreach ($validatedData['clients'] as $clientData) {
                $client = new Client();
                $client->first_name = $clientData['first_name'];
                $client->last_name = $clientData['last_name'];
                $client->email = $clientData['email'];
                $client->save();
                $reservation->clients()->attach($client);
            }
        }
    }
}
